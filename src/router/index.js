import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
  path: '/main',
  name: 'index',
  component: () => import('../views/index.vue')
},
  {
    path: '/m1',
    name: 'm1',
    component: () => import('../views/m1/m1.vue')
  },
  {
    path: '/m2',
    name: 'm2',
    component: () => import('../views/m2/m2.vue')
  },
  {
    path: '/m3',
    name: 'm3',
    component: () => import('../views/m3/m3.vue')
  },
  {
    path: '/m4',
    name: 'm4',
    component: () => import('../views/m4/m4.vue')
  }

  ]
const router = new VueRouter({
  routes
})

export default router
