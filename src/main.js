import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import dataV from '@jiaminghi/data-view';
import ItemWrap from './components/item-wrap/item-wrap.vue'
import FlashTitle from './components/flash-title/flash-title'
import FlashTitle2 from './components/flash-title/flash-title2'
// 引入全局css
import './assets/scss/style.scss';
// 按需引入vue-awesome图标
import Icon from 'vue-awesome/components/Icon';
import 'vue-awesome/icons/chart-bar.js';
import 'vue-awesome/icons/chart-area.js';
import 'vue-awesome/icons/chart-pie.js';
import 'vue-awesome/icons/chart-line.js';
import 'vue-awesome/icons/align-left.js';
import Antd, { version } from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css'; // or 'ant-design-vue/dist/antd.less'
console.log('ant-design-vue version:', version)

//引入echart
//4.x 引用方式
import echarts from 'echarts'
//5.x 引用方式为按需引用
//希望使用5.x版本的话,需要在package.json中更新版本号,并切换引用方式
//import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false;

// 全局注册
Vue.component('icon', Icon);
Vue.component("ItemWrap",ItemWrap)
Vue.component("FlashTitle",FlashTitle)
Vue.component("FlashTitle2",FlashTitle2)
Vue.use(dataV);
Vue.use(Antd)

import vueFlvPlayer from 'vue-flv-player'
Vue.use(vueFlvPlayer)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
