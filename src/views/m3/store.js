var store = {
  data:{mosList:[],lineList:[],qcList:{},machineSummaryInfos:[]},
  machineMap:[
    {
      lineMachine:{equipmentName:"吸<br/>板<br/>机",
        lightLocationLeft:70,
        lightLocationTop:1780,
        rate:1,qtyOk:2592,qtyErr:0
      }
    }
  ],
  qcLists:[],
  qcList: {
    qcDefectTrans:[
      {
        station:'初测ATE',
        import1ProdModel:'这是个严重问题221，初测ATE',
        import1PeDesc:'PE分析啊咧，发送多个看这是个常规问题。接着这是一大串分析原因说',
        import1Countermeasure:'改善对策是这样的吧，详细请看啊发生的阿斯顿阿桑的歌',
        import2ProdModel:'这是个严重问题2，初测ATE',
        import2PeDesc:'PE分析啊咧，发送多个看这是个常规问题。接着这是一大串分析原因说明，藏品爱上了大噶是快乐的梦工厂克拉斯但是一啊实打实公开拉升的给工杉原杏璃茶杯犬',
        import2Countermeasure:'改善对策是这样的吧，详细请看啊发生的阿斯顿阿桑的歌，基本呀顶号 ；基本面事；基本号工在阿斯达克开始卡斯卡迪思想界西南非栛枯'
      },
      {
        station:'炉后AOI',
        import1ProdModel:'这是个严重问题1，炉后AOI',
        import1PeDesc:'PE分析啊咧，发送多个看这是个常规问题。炉后AOI',
        import1Countermeasure:'改善对策是这样的吧，详细请看啊发生的阿斯顿阿桑的歌，基本呀顶号 ；基本面事；基本号工在阿斯达克开始卡斯卡迪思想界西南非栛枯',
        import2ProdModel:'这是个严重问题2，炉后AOI',
        import2PeDesc:'PE分析啊咧，发送多个看这是个常规问题。接着这是一大串分析原因说明，藏品爱上了大噶是快乐的梦',
        import2Countermeasure:'改善对策是这样的吧，详细请看啊发生的阿斯顿阿桑的歌，基本呀顶号 ；基本面事；基本号工在阿斯达克开始卡斯卡迪思想界西南非栛枯'
      },
    ],
    category: [
      "11-18",
      "11-19",
      "11-20",
      "11-21",
      "11-22",
      "11-23"
    ],
    aoi1Data: [
      95.3,
      92.3,
      91.3,
      96.3,
      93.62,
      97.2
      /*0.953,
      0.923,
      0.913,
      0.963,
      0.9362,
      0.972*/
    ],
    aoi2Data: [
      93.3,
      90.3,
      95.3,
      96.3,
      93.62,
      97.2
    ],
    volData: [
      92.32,
      93.16,
      94.3,
      95.3,
      97.62,
      93.28
      /*0.923,
      0.933,
      0.943,
      0.953,
      0.9762,
      0.932*/
    ],
    ate1Data: [
      94.13,
      96.23,
      94.67,
      93.21,
      98.62,
      97.27
      /*0.943,
      0.963,
      0.923,
      0.933,
      0.9862,
      0.912*/
    ],
    ate2Data: [
      94.13,
      96.23,
      92.67,
      97.21,
      98.62,
      99.27
      /*0.943,
      0.963,
      0.923,
      0.933,
      0.9862,
      0.912*/
    ],
    rateData: [
      95.2,
      96.13,
      94.3,
      91.3,
      94.5,
      96.2
    ]
  },

  curLine:0,
  curMI:0,
  miCount:0,
  lineCount:0,
  setData(resData){
    this.data=resData
    this.qcLists=this.data.qcData
    this.qcList=this.qcLists[this.curLine]
    this.miCount=this.qcList.miList.length
    this.lineCount=this.qcLists.length
    this.machineMap=this.data.machineSummaryInfos
    this.curMI=-1
    this.nextMI()
  },
  nextLine(){
    if (this.curLine<this.lineCount-1){
      this.curLine=this.curLine+1
    }
    else{
      this.curLine=0
    }
    this.qcList=this.qcLists[this.curLine]
    this.curMI=0
    this.miCount=this.qcList.miList.length
    if (this.miCount>0){
      this.miList=this.qcList.miList[this.curMI]
    }
    console.log('switch line:'+this.qcList.pline)
  },
  miList:{},
  nextMI(){
    if (this.miCount.length==0) {
      this.miList = null
    }
    if (this.curMI<this.miCount-1){
      this.curMI=this.curMI+1
    }
    else{
      this.curMI=0
    }
    this.miList=this.qcList.miList[this.curMI]
    console.log('switch mi:'+this.miList)
  },
  fmtNum(n,s,f){
    let str,xs
    n=n+''
    if (n.includes('.')){
      str=n.split('.')[0]
      xs=n.split('.')[1]
    }
    else{
      xs=''
      str=n
    }
    var re=/(?=(?!(\b))(\d{3})+$)/g;
    str=str.replace(re,",");
    if (xs!=''){
      str=str+'.'+xs
    }
    return (((f?f:" ").repeat(s)) + str).slice(-s)
  },
}
export default store
